import os

def str_to_bool(value: str):
    if value.lower() == 'true':
        return True
    return False

DEBUG: bool = str_to_bool(os.getenv("DEBUG", "False"))
DEBUG_LOG: bool = str_to_bool(os.getenv("DEBUG_LOG", "False"))
RUN_ON_STARTUP: bool = str_to_bool(os.getenv("RUN_ON_STARTUP", "False"))
SCHEDULED_RUN: bool = str_to_bool(os.getenv("SCHEDULED_RUN", "True"))
SCHEDULED_HOUR: int = int(os.getenv("SCHEDULED_HOUR", "10"))
SCHEDULED_MINUTE = int(os.getenv("SCHEDULED_MINUTE", "0"))

FORECAST_HOURS: int = int(os.getenv("FORECAST_HOURS", 72))

CDSAPI_FOLDER: str = os.getenv("CDSAPI_FOLDER", "/root/")
GALILEO_CLOUD_URL_BASE: str = os.getenv("GALILEO_CLOUD_URL_BASE", "https://whatever/")

MQTT_HOST: str = os.getenv("MQTT_HOST", "my.mqtthost.com")
MQTT_PORT: int = int(os.getenv("MQTT_PORT", 1883))
MQTT_USERNAME: str = os.getenv("MQTT_USERNAME", "user")
MQTT_PASSWORD: str = os.getenv("MQTT_PASSWORD", "password")