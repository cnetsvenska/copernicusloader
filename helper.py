from datetime import datetime
import requests
import environment
import json
import logging
import sys
import math

class Point:

    def __init__(self, latitude: float, longitude: float):
        self.latitude = latitude
        self.longitude = longitude

class BoundingBox:
    
    def __init__(self, start: Point, end: Point):
        self.start = start
        self.end = end

class MeasurementMetadata:

    __dist_step = 0.1

    bounding_box: BoundingBox = None
    datastream_ID: int = None
    feature_of_interest_ID: int = None
    measurement_type: str = None
    multiplier: int = None
    addition: float = None

    def __init__(self, datastream_ID: int, feature_of_interest_ID: int, measurement_type: str, multiplier: int = 1, addition: float = 0, bb_min_distance: float = None):
        self.datastream_ID = datastream_ID
        self.feature_of_interest_ID = feature_of_interest_ID
        self.measurement_type = measurement_type
        self.multiplier = multiplier
        self.addition = addition

        self.populate_bounding_box()
        if bb_min_distance is not None:
            self.calc_min_bb_distance(bb_min_distance)
    
    def populate_bounding_box(self):
        bb = BoundingBox(Point(0, 0), Point(0, 0))
        coords = []
        url = environment.GALILEO_CLOUD_URL_BASE + "FeaturesOfInterest({})".format(self.feature_of_interest_ID)
        response = requests.get(url)
        if response is not None:
            content = response.json()
            if content is not None:
                if "feature" in content:
                    if "coordinates" in content["feature"]:
                        if len(content["feature"]["coordinates"]) > 0:
                            coords = content["feature"]["coordinates"][0]
            response.close()

        if len(coords) > 0:
            bb.start.latitude = coords[0][1]
            bb.end.latitude = coords[0][1]
            bb.start.longitude = coords[0][0]
            bb.end.longitude = coords[0][0]
        for coord in coords:
            if coord[1] < bb.start.latitude:
                bb.start.latitude = coord[1]
            if coord[1] > bb.end.latitude:
                bb.end.latitude = coord[1]

            if coord[0] < bb.start.longitude:
                bb.start.longitude = coord[0]
            if coord[0] > bb.end.longitude:
                bb.end.longitude = coord[0]

        self.bounding_box = bb

    def calc_min_bb_distance(self, min_dist: float):
        if self.bounding_box is None:
            return
        
        end = math.floor(self.bounding_box.end.latitude / min_dist) * min_dist
        start = math.ceil(self.bounding_box.start.latitude / min_dist) * min_dist
        while((self.bounding_box.end.latitude - self.bounding_box.start.latitude) < min_dist or
            (end - start) < min_dist):
            self.bounding_box.end.latitude += self.__dist_step
            self.bounding_box.start.latitude -= self.__dist_step
            end = math.floor(self.bounding_box.end.latitude / min_dist) * min_dist
            start = math.ceil(self.bounding_box.start.latitude / min_dist) * min_dist

        end = math.floor(self.bounding_box.end.longitude / min_dist) * min_dist
        start = math.ceil(self.bounding_box.start.longitude / min_dist) * min_dist
        while((self.bounding_box.end.longitude - self.bounding_box.start.longitude) < min_dist or 
            (end - start) < min_dist):
            self.bounding_box.end.longitude += self.__dist_step
            self.bounding_box.start.longitude -= self.__dist_step
            end = math.floor(self.bounding_box.end.longitude / min_dist) * min_dist
            start = math.ceil(self.bounding_box.start.longitude / min_dist) * min_dist
        
        
        self.bounding_box.start.latitude = int(self.bounding_box.start.latitude * 100000) / 100000
        self.bounding_box.start.longitude = int(self.bounding_box.start.longitude * 100000) / 100000
        self.bounding_box.end.latitude = int(self.bounding_box.end.latitude * 100000) / 100000
        self.bounding_box.end.longitude = int(self.bounding_box.end.longitude * 100000) / 100000
        

def get_atmosphere_measurement_metadata_list():
    measurement_metadata_list = []

    # Vasastan
    # NO2
    measurement_metadata_list.append(MeasurementMetadata(746, 30, "NO2", 10**9, 0, 0.1))
    # O3
    measurement_metadata_list.append(MeasurementMetadata(747, 30, "O3", 10**9, 0, 0.1))
    # PM10
    measurement_metadata_list.append(MeasurementMetadata(748, 30, "PM10", 10**9, 0, 0.1))
    # PM25
    measurement_metadata_list.append(MeasurementMetadata(749, 30, "PM2.5", 10**9, 0, 0.1))
    # Grass Pollen
    measurement_metadata_list.append(MeasurementMetadata(37628, 30, "Grass Pollen", 1, 0, 0.1))
    # Birch Pollen
    measurement_metadata_list.append(MeasurementMetadata(37624, 30, "Birch Pollen", 1, 0, 0.1))

    # Täby
    # NO2
    measurement_metadata_list.append(MeasurementMetadata(750, 4, "NO2", 10**9, 0, 0.1))
    # O3
    measurement_metadata_list.append(MeasurementMetadata(751, 4, "O3", 10**9, 0, 0.1))
    # PM10
    measurement_metadata_list.append(MeasurementMetadata(752, 4, "PM10", 10**9, 0, 0.1))
    # PM25
    measurement_metadata_list.append(MeasurementMetadata(753, 4, "PM2.5", 10**9, 0, 0.1))
    # Grass Pollen
    measurement_metadata_list.append(MeasurementMetadata(37626, 4, "Grass Pollen", 1, 0, 0.1))
    # Birch Pollen
    measurement_metadata_list.append(MeasurementMetadata(37622, 4, "Birch Pollen", 1, 0, 0.1))

    # Lidingö
    # NO2
    measurement_metadata_list.append(MeasurementMetadata(754, 241, "NO2", 10**9, 0, 0.1))
    # O3
    measurement_metadata_list.append(MeasurementMetadata(755, 241, "O3", 10**9, 0, 0.1))
    # PM10
    measurement_metadata_list.append(MeasurementMetadata(756, 241, "PM10", 10**9, 0, 0.1))
    # PM25
    measurement_metadata_list.append(MeasurementMetadata(757, 241, "PM2.5", 10**9, 0, 0.1))
    # Grass Pollen
    measurement_metadata_list.append(MeasurementMetadata(37627, 241, "Grass Pollen", 1, 0, 0.1))
    # Birch Pollen
    measurement_metadata_list.append(MeasurementMetadata(37623, 241, "Birch Pollen", 1, 0, 0.1))

    # Rådhuspladsen
    # NO2
    measurement_metadata_list.append(MeasurementMetadata(7904, 335, "NO2", 10**9, 0, 0.1))
    # O3
    measurement_metadata_list.append(MeasurementMetadata(13846, 335, "O3", 10**9, 0, 0.1))
    # PM10
    measurement_metadata_list.append(MeasurementMetadata(31673, 335, "PM10", 10**9, 0, 0.1))
    # PM25
    measurement_metadata_list.append(MeasurementMetadata(37617, 335, "PM2.5", 10**9, 0, 0.1))
    # Grass Pollen
    measurement_metadata_list.append(MeasurementMetadata(37629, 335, "Grass Pollen", 1, 0, 0.1))
    # Birch Pollen
    measurement_metadata_list.append(MeasurementMetadata(37625, 335, "Birch Pollen", 1, 0, 0.1))


    return measurement_metadata_list

def get_climate_measurement_metadata_list():
    measurement_metadata_list = []

    # Vasastan
    # Temperatur
    measurement_metadata_list.append(MeasurementMetadata(37620, 30, "2m Temperature", 1, -272.15, 1.5))
    # Solar Radiation
    measurement_metadata_list.append(MeasurementMetadata(45919, 30, "Solar Radiation", 1/3600, 0, 1.5))

    # Täby
    # Temperatur
    measurement_metadata_list.append(MeasurementMetadata(37618, 4, "2m Temperature", 1, -272.15, 1.5))
    # Solar Radiation
    measurement_metadata_list.append(MeasurementMetadata(62548, 30, "Solar Radiation", 1/3600, 0, 1.5))
    
    # Lidingö
    # Temperatur
    measurement_metadata_list.append(MeasurementMetadata(37619, 241, "2m Temperature", 1, -272.15, 1.5))
    # Solar Radiation
    measurement_metadata_list.append(MeasurementMetadata(70864, 30, "Solar Radiation", 1/3600, 0, 1.5))

    # Rådhuspladsen
    # Temperatur
    measurement_metadata_list.append(MeasurementMetadata(37621, 335, "2m Temperature", 1, -272.15, 1.5))
    # Solar Radiation
    measurement_metadata_list.append(MeasurementMetadata(79181, 30, "Solar Radiation", 1/3600, 0, 1.5))

    return measurement_metadata_list

def result_to_json(value, value_type, phenomenon_time, result_time, feature_of_interest_ID):
    __value_type_table = {
        'NO2': 'NO2',
        'O3': 'O3',
        'PM10': 'PM10',
        'PM2.5': 'PM2.5',
        'Birch Pollen': 'BP',
        'Grass Pollen': 'GP',
        '2m Temperature': 'Temperature',
        'Solar Radiation': 'SR'
    }

    result = {
        "phenomenonTime": phenomenon_time,
        "resultTime": result_time,
        "FeatureOfInterest": {
            "@iot.id": feature_of_interest_ID
        },
        "result": {
            "response": {
                "value": value
            },
            "valueType": __value_type_table.get(value_type, value_type)
        }
    }
    
    return json.dumps(result)

def log(message:str, level:str):
    if environment.DEBUG_LOG:
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s: %(message)s')
        logging.log(logging._nameToLevel[level], message)
    else:
        print("{} {}: {}".format(datetime.now(), level, message))
        sys.stdout.flush()

def log_debug(message: str):
    log(message, "DEBUG")

def log_info(message: str):
    log(message, "INFO")

def log_warning(message: str):
    log(message, "WARNING")

def log_error(message: str):
    log(message, "ERROR")

def log_critical(message: str):
    log(message, "CRITICAL")