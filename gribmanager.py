import requests
import os
from helper import BoundingBox
from datetime import datetime, timedelta
import xarray as xr
import environment
import helper
import cdsapi
import logging

class GribManager:

    __foldername = "grib"
    __errorfoldername = "error"

    __convert_table = {
        'NO2': 'nitrogen_dioxide',
        'O3': 'ozone',
        'PM10': 'particulate_matter_10um',
        'PM2.5': 'particulate_matter_2.5um',
        'Birch Pollen': 'birch_pollen',
        'Grass Pollen': 'grass_pollen',
        '2m Temperature': '2m_temperature',
        'Solar Radiation': 'surface_net_solar_radiation'
    }

    __param_table = {
        '2m Temperature': 't2m',
        'Solar Radiation': 'ssr'
    }

    # Constructor
    def __init__(self):
        if environment.DEBUG_LOG:
            logging.getLogger('cdsapi').setLevel(logging.DEBUG)
            logging.getLogger('cfgrib').setLevel(logging.DEBUG)
        else:
            logging.getLogger('cdsapi').setLevel(logging.ERROR)
            logging.getLogger('cfgrib').setLevel(logging.ERROR)

        
    # Download File
    def download_atmosphere_file(self, body, measurement_type):
        # Create folder if it does not exist
        if not os.path.exists(self.__foldername):
            os.makedirs(self.__foldername)

        c = cdsapi.Client()
        c.retrieve(
            'cams-europe-air-quality-forecasts',
            body,
            '{}/{}.grib'.format(self.__foldername, measurement_type)
        )

    def download_climate_file(self, body, measurement_type):
        # Create folder if it does not exist
        if not os.path.exists(self.__foldername):
            os.makedirs(self.__foldername)
        
        c = cdsapi.Client()
        c.retrieve(
            'seasonal-original-single-levels',
            body,
            '{}/{}.grib'.format(self.__foldername, measurement_type)
        )

    # Open File
    def open_file(self):
        pass

    # Delete Files
    def delete_files(self):
        if os.path.exists(self.__foldername):
            file_list = os.listdir(self.__foldername)
            for filename in file_list:
                os.remove("{}/{}".format(self.__foldername, filename))
            os.rmdir(self.__foldername)


    # Save current grib file as an txt in an error folder
    def save_error_file(self, url, measurement_type):
        try:
            # Read content from current file
            filename = "{}.grib".format(measurement_type)
            f = open("{}/{}".format(self.__foldername, filename), "r")
            content = f.read()
            f.close()

            # Add url to the error file
            content = url + "\n\n" + content

            # Create folder if it does not exist
            if not os.path.exists(self.__errorfoldername):
                os.makedirs(self.__errorfoldername)
                
            # Save as a text file current current time in the error folder
        
            error_filename = "{}.txt".format(datetime.now())
            error_filename = error_filename.replace(" ", "_")
            error_filename = error_filename.replace(":", "-")
            error_file = open("{}/{}".format(self.__errorfoldername, error_filename), "w")
            error_file.write(content)
            error_file.close()
        except Exception as e:
            helper.log_error("Could not save log of error file")
            helper.log_error(e)


    # Build the request body for fetching Copernicus atmosphere data
    def build_atmosphere_body(self, measurement_type, start_time, leadtime_hour, lat_start, lat_end, long_start, long_end):
        body = {
            'model': 'ensemble',
            'date': '{}/{}'.format(start_time, start_time),
            'format': 'grib',
            'area': [
                lat_end, long_start, lat_start, long_end,
            ],
            'leadtime_hour': '{}'.format(leadtime_hour),
            'time': '00:00',
            'type': 'forecast',
            'level': '0',
            'variable': self.__convert_table[measurement_type],
        }

        return body

    # Build the request body for fetching Copernicus atmosphere data
    def build_climate_body(self, measurement_type, start_year, start_month, start_day, leadtime_hour, lat_start, lat_end, long_start, long_end):
        body = {
            'year': '{}'.format(start_year),
            'month': '{}'.format(start_month),
            'day': '{}'.format(start_day),
            'format': 'grib',
            'area': [
                lat_end, long_start, lat_start, long_end,
            ],
            'leadtime_hour': '{}'.format(leadtime_hour),
            'variable': self.__convert_table[measurement_type],
            'originating_centre': 'ukmo',
            'system': '15'
        }

        return body

    # Get All Filenames
    def get_filenames(self):
        file_list = []
        if os.path.exists(self.__foldername):
            file_list = os.listdir(self.__foldername)
        return file_list

    # Get Average Atmosphere Value in the area
    def get_atmosphere_avg(self, measurement_type):
        average = 0.0
        gribfile = xr.open_dataset("{}/{}.grib".format(self.__foldername, measurement_type), engine="cfgrib", backend_kwargs={'indexpath': ''})
        for lat_i in range(gribfile.latitude.size):
            for lng_i in range(gribfile.longitude.size):
                val = gribfile.paramId_0[lat_i][lng_i].values
                average += val

        average = average / (gribfile.latitude.size * gribfile.longitude.size)
        return average

    # Get Average Climate Value in the area
    def get_climate_avg(self, measurement_type):
        average = 0.0
        gribfile = xr.open_dataset("{}/{}.grib".format(self.__foldername, measurement_type), engine="cfgrib", backend_kwargs={'indexpath': ''})
        for n in range(gribfile.number.size):
            for lat_i in range(gribfile.latitude.size):
                for lng_i in range(gribfile.longitude.size):
                    val = gribfile[self.__param_table.get(measurement_type, 'paramId_0')][n][lat_i][lng_i].values
                    average += val

        average = average / (gribfile.number.size * gribfile.latitude.size * gribfile.longitude.size)
        return average

    # Get Min/Max values (for testing)
    def get_min_max(self, measurement_type):
        gribfile = xr.open_dataset("{}/{}.grib".format(self.__foldername, measurement_type), engine="cfgrib", backend_kwargs={'indexpath': ''})
        min_val = {"lat": gribfile.latitude[0].values, "lng": gribfile.longitude[0].values, "val": gribfile.paramId_0[0][0].values}
        max_val = {"lat": gribfile.latitude[0].values, "lng": gribfile.longitude[0].values, "val": gribfile.paramId_0[0][0].values}
        for lat_i in range(gribfile.latitude.size):
            percent = (lat_i / gribfile.latitude.size) * 100
            if environment.DEBUG_LOG:
                helper.log_debug("{}%".format(int(percent)))
            for lng_i in range(gribfile.longitude.size):
                val = gribfile.paramId_0[lat_i][lng_i]
                if val.values < min_val["val"]:
                    min_val["lat"] = val.latitude.values
                    min_val["lng"] = val.longitude.values
                    min_val["val"] = val.values
                if val.values > max_val["val"]:
                    max_val["lat"] = val.latitude.values
                    max_val["lng"] = val.longitude.values
                    max_val["val"] = val.values
        if environment.DEBUG_LOG:
            helper.log_debug("100%")
        return min_val, max_val
        
    # Get Start/End Time for the URL
    def get_times_from_day(self, leadtime_hour):
        now = datetime.now()
        midnight = now.replace(hour=0, minute=0, second=0, microsecond=0)
        future = midnight + timedelta(hours=leadtime_hour)
        start_date = midnight.strftime("%Y-%m-%d")
        end_date = future.strftime("%Y-%m-%dT%H:00:00Z")

        return start_date, end_date

    # Get Start/End Time for the URL
    def get_times_from_month(self, added_hour):
        now = datetime.now()
        midnight = now.replace(hour=0, minute=0, second=0, microsecond=0)
        # Use last month as starting point if we are within the first days of the month
        if now.day < 15:
            now = now + timedelta(days=-15)
        current_month = now.replace(day=1, hour=0, minute=0, second=0, microsecond=0)

        start_year = current_month.year
        start_month = current_month.month
        start_day = current_month.day
        leadtime_hour = ((midnight - current_month).days * 24) + added_hour

        future = midnight + timedelta(hours=added_hour)
        start_date = midnight.strftime("%Y-%m-%d")
        end_date = future.strftime("%Y-%m-%dT%H:00:00Z")

        return start_year, start_month, start_day, leadtime_hour, end_date

    # Get Bounding Box (For testing)
    def get_bounding_box(self):
        latitude_start = "58.00"
        latitude_end = "61.00"
        longitude_start = "15.00"
        longitude_end = "20.00"

        bb = BoundingBox(latitude_start, latitude_end, longitude_start, longitude_end)
        return bb