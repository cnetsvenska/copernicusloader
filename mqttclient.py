import paho.mqtt.client as mqtt
import environment
import helper

class MqttClient:
    
    __client = None # mqtt client

    def __init__(self):
        self.__client = mqtt.Client()
        if environment.DEBUG_LOG:
            self.__client.on_connect = self.__on_connect
            self.__client.on_message = self.__on_message
            self.__client.on_log = self.__on_log
            self.__client.on_publish = self.__on_publish

        self.__client.username_pw_set(environment.MQTT_USERNAME, environment.MQTT_PASSWORD)
        self.__client.connect(environment.MQTT_HOST, environment.MQTT_PORT)
        self.__client.loop_start()

    def disconnect(self):
        self.__client.loop_stop()
        self.__client.disconnect()

    def publish(self, topic: str, payload: str):
        self.__client.publish(topic=topic, payload=payload, qos=2)

    def __on_connect(self, client, userdata, flags, rc):
        helper.log_debug("Connected with result code {}".format(str(rc)))

    def __on_message(self, client, userdata, msg):
        helper.log_debug("{}: {}".format(msg.topic, msg.payload))

    def __on_log(self, client, userdata, level, buf):
        helper.log_debug("Log: {}".format(buf))

    def __on_publish(self, client, userdata, mid):
        helper.log_debug("On Publish: {}".format(mid))