FROM python:3

WORKDIR /usr/src/

RUN apt-get update
RUN apt-get -y install libeccodes0

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "./main.py" ]