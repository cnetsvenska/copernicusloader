## Copernicus Loader Implementation

The COPERNICUS Connector collects automatically data from Copernicus Atmosphere Monitoring Service (CAMS). The data collected is forecast data for the following parameters: NO2, O3, PM10, PM2.5, Grass Pollen, Birch Pollen, Temperature and Solar Radiation. The COPERNICUS Connector is a Docker based component developed in Python 3.7.

![GOEASY](https://bitbucket.org/cnetsvenska/copernicusloader/raw/master/images/copernicusloader.png)

The COPERNICUS Connector components are shown in Figure 20 together with the connections to other GOEASY cloud components. The components have the following functionality:

* GRIB Collector: Retrieves the CAMS forecast information once every night using CAMS public API. The information is in a compact Gridded Binary (GRIB) file format.
* GRIB Format Parser: Parses the GRIB files and extracts the information that is requested and creates simple Observations.
* OGC SensorThings Formatter: Creates OGC Sensorthings API compliant Observations. It uses the Resource Catalogue Manager  [Resource Catalogue Manager](https://bitbucket.org/cnetsvenska/resource-catalogue-manager/src/master//) for getting the mapping to the correct datastreams. Finally, it publishes the OGC Observation to the Message Broker 



## Running the Copernicus Loader server

The Copernicus Loader server is deployed as a docker container
Build the container and deploy it using the docker.
NB! You need to set the mqtt parameters in environment.py

## Affiliation
![GOEASY](https://bitbucket.org/cnetsvenska/nb-iotudpserver/raw/master/images/GOEASY_HD_Logo2.png)
This work is supported by the European Commission through the [GOEASY H2020 PROJECT](https://goeasyproject.eu/) under grant agreement No 776261.