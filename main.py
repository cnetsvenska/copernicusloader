from gribmanager import GribManager
from mqttclient import MqttClient
from helper import MeasurementMetadata
from datetime import datetime, timedelta
import helper
import json
import environment
import sched
import time

grib: GribManager = None
mqttclient: MqttClient = None

def init():
    global grib
    global mqttclient
    grib = GribManager()
    mqttclient = None
    if not environment.DEBUG:
        mqttclient = MqttClient()

def cleanup():
    global grib
    global mqttclient
    if mqttclient is not None:
        mqttclient.disconnect()

    if grib is not None:
        grib.delete_files()

def copy_cdsapi_file(from_filename: str):
    # Read content
    f = open(from_filename, "r")
    content = f.read()
    f.close()

    # Write to file
    f = open("{}/.cdsapirc".format(environment.CDSAPI_FOLDER), "w")
    f.write(content)
    f.close()

def print_result(value, measurement_type, end_time, result_time, feature_of_interest_ID, datastream_ID):
    payload = helper.result_to_json(value, measurement_type, end_time, result_time, feature_of_interest_ID)
    topic = "GOST/Datastreams({})/Observations".format(datastream_ID)

    # Print to console
    helper.log_info(topic)
    helper.log_info(payload)
    helper.log_info("-----------------------------------")

def write_to_gost(value, measurement_type, end_time, result_time, feature_of_interest_ID, datastream_ID, n, total):
    global mqttclient
    if mqttclient is None:
        helper.log_error("MQTT client not initialized")
        return

    try:
        # Write to GOST
        topic = "GOST/Datastreams({})/Observations".format(datastream_ID)
        payload = helper.result_to_json(value, measurement_type, end_time, result_time, feature_of_interest_ID)
        mqttclient.publish(topic, payload)

        if environment.DEBUG_LOG:
            # Print to console
            helper.log_debug(topic)
            helper.log_debug(payload)
    except Exception as e:
        helper.log_error("Error sending data (Data: {}, Datastream: {}, Time: {})".format(measurement_type, datastream_ID, end_time))
        helper.log_error(e)
        grib.delete_files()

    if environment.DEBUG_LOG:
        if n is not None and total is not None:
            # Print progress
            percent = (n / total) * 100
            helper.log_debug("{}%".format(int(percent)))

def atmosphere_loop():
    # Initialize
    global grib
    global mqttclient
    init()
    copy_cdsapi_file(".cdsapircads")
    measurement_metadata_list = helper.get_atmosphere_measurement_metadata_list()
    measurement_metadata: MeasurementMetadata = None    
    n = 0
    total = len(measurement_metadata_list) * environment.FORECAST_HOURS
    result_time = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    helper.log_info("Populating new Copernicus atmosphere data at: {}".format(result_time))

    # Start Loop for all datastreams and measurements types
    for measurement_metadata in measurement_metadata_list:
        helper.log_info("Populating datastream: {}".format(measurement_metadata.datastream_ID))
        # Make a request for every hour
        for leadtime_hour in range(environment.FORECAST_HOURS):
            # Build request
            start_time, end_time = grib.get_times_from_day(leadtime_hour)
            bb = measurement_metadata.bounding_box
            body = grib.build_atmosphere_body(measurement_metadata.measurement_type, start_time, leadtime_hour, bb.start.latitude, bb.end.latitude, bb.start.longitude, bb.end.longitude)

            try:
                # Download File
                grib.download_atmosphere_file(body, measurement_metadata.measurement_type)
            except Exception as e:
                helper.log_error("Error downloading file (Data: {}, Datastream: {}, Time: {})".format(measurement_metadata.measurement_type, measurement_metadata.datastream_ID, end_time))
                helper.log_error(e)
                helper.log_error(json.dumps(body))
                continue

            # Read files
            try:
                avg_val = grib.get_atmosphere_avg(measurement_metadata.measurement_type)
                avg_val *= measurement_metadata.multiplier
                avg_val += measurement_metadata.addition
            except Exception as e:
                helper.log_error("Error getting values from files (Data: {}, Datastream: {}, Time: {})".format(measurement_metadata.measurement_type, measurement_metadata.datastream_ID, end_time))
                helper.log_error(e)
                helper.log_error(json.dumps(body))
                continue
            

            # Send Result
            if environment.DEBUG:
                print_result(avg_val, measurement_metadata.measurement_type, end_time, result_time, 
                    measurement_metadata.feature_of_interest_ID, measurement_metadata.datastream_ID)
            else:
                n += 1
                write_to_gost(avg_val, measurement_metadata.measurement_type, end_time, result_time, 
                    measurement_metadata.feature_of_interest_ID, measurement_metadata.datastream_ID,
                    n, total)

            # Remove used files
            grib.delete_files()


    # Cleanup
    cleanup()
    helper.log_info("Done populating Copernicus data")

def climate_loop():
    # Initialize
    global grib
    global mqttclient
    init()
    copy_cdsapi_file(".cdsapirccds")
    measurement_metadata_list = helper.get_climate_measurement_metadata_list()
    measurement_metadata: MeasurementMetadata = None    
    n = 0
    total = len(measurement_metadata_list) * environment.FORECAST_HOURS
    result_time = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    helper.log_info("Populating new Copernicus Climate data at: {}".format(result_time))

    # Start Loop for all datastreams and measurements types
    for measurement_metadata in measurement_metadata_list:
        helper.log_info("Populating datastream: {}".format(measurement_metadata.datastream_ID))
        prevValue = None
        # Make a request for every 6 hour
        for added_hour in range(-24, environment.FORECAST_HOURS, 6):
            # Exception for solar
            if measurement_metadata.measurement_type == "Solar Radiation":
                if added_hour % 24 != 0:
                    continue
            # Exception for Temperature
            if measurement_metadata.measurement_type == "2m Temperature":
                if added_hour < 0:
                    continue

            # Build request
            start_year, start_month, start_day, leadtime_hour, end_time = grib.get_times_from_month(added_hour)
            bb = measurement_metadata.bounding_box
            body = grib.build_climate_body(measurement_metadata.measurement_type, 
                start_year, start_month, start_day, leadtime_hour, 
                bb.start.latitude, bb.end.latitude, bb.start.longitude, bb.end.longitude)

            try:
                # Download File
                grib.download_climate_file(body, measurement_metadata.measurement_type)
            except Exception as e:
                helper.log_error("Error downloading file (Data: {}, Datastream: {}, Time: {})".format(measurement_metadata.measurement_type, measurement_metadata.datastream_ID, end_time))
                helper.log_error(e)
                helper.log_error(json.dumps(body))
                continue

            # Read files
            try:
                avg_val = grib.get_climate_avg(measurement_metadata.measurement_type)
                avg_val *= measurement_metadata.multiplier
                avg_val += measurement_metadata.addition
                
                if measurement_metadata.measurement_type == "Solar Radiation":
                    oldPrevValue = prevValue
                    prevValue = avg_val
                    if oldPrevValue is None:
                        continue
                    else:
                        avg_val = prevValue - oldPrevValue
            except Exception as e:
                helper.log_error("Error getting values from files (Data: {}, Datastream: {}, Time: {})".format(measurement_metadata.measurement_type, measurement_metadata.datastream_ID, end_time))
                helper.log_error(e)
                helper.log_error(json.dumps(body))
                continue
            

            # Send Result
            if environment.DEBUG:
                print_result(avg_val, measurement_metadata.measurement_type, end_time, result_time, 
                    measurement_metadata.feature_of_interest_ID, measurement_metadata.datastream_ID)
            else:
                n += 1
                write_to_gost(avg_val, measurement_metadata.measurement_type, end_time, result_time, 
                    measurement_metadata.feature_of_interest_ID, measurement_metadata.datastream_ID,
                    n, total)

            # Remove used files
            grib.delete_files()


    # Cleanup
    cleanup()
    helper.log_info("Done populating Copernicus data")

def main_loop():
    atmosphere_loop()
    climate_loop()

helper.log_info("Starting Service")

scheduler = sched.scheduler(time.time, time.sleep)

if environment.RUN_ON_STARTUP:
    main_loop()

try:
    while(environment.SCHEDULED_RUN):
        # Calculate time for next loop
        now = datetime.now()
        next_loop = datetime(now.year, now.month, now.day, environment.SCHEDULED_HOUR, environment.SCHEDULED_MINUTE, 0, 0)
        if now > next_loop:
            next_loop = next_loop + timedelta(days=1)
        time_to_next_loop = (next_loop - now).total_seconds()
        helper.log_info("Next loop starts at {} in {} seconds".format(next_loop, time_to_next_loop))

        # Scheduele next loop
        eventID = scheduler.enter(time_to_next_loop, 1, main_loop)
        scheduler.run()
except (KeyboardInterrupt, SystemExit):
    cleanup()
    helper.log_info("Interrupted service")

helper.log_info("Service ended")